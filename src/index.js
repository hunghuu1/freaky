function scrollToElement(idclick,idbutton){
    if(typeof idclick === 'string' && typeof idbutton === 'string'){
        let click = document.querySelector(idclick);
        let button = document.querySelector(idbutton);

        if(click && button){
            click.addEventListener('click',function(){
                button.scrollIntoView({
                    behavior: "smooth", 
                    block: "start", 
                    inline: "nearest",
                });
            })
        }
    }
}

scrollToElement('.banner__scroll', '#item');

function MenuMobile(idbutton,idmenu){
    if(typeof idbutton ==='string' && typeof idmenu === 'string'){
        let button = document.querySelector(idbutton);
        let menu = document.querySelector(idmenu);

        if(button && menu){
            button.addEventListener('click',function(){
                menu.classlist.toggle('active')
            })
        }
    }
}



MenuMobile('#tem','#navBtn');

